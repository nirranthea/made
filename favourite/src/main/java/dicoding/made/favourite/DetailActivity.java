package dicoding.made.favourite;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.favourite.model.MovieItem;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.detail_backdrop) ImageView imgBackdrop;
    @BindView(R.id.detail_profpic) ImageView imgPoster;
    @BindView(R.id.detail_title) TextView tvTitle;
    @BindView(R.id.detail_date) TextView tvDate;
    @BindView(R.id.detail_language) TextView tvLang;
    @BindView(R.id.detail_rate) TextView tvRate;
    @BindView(R.id.detail_desc) TextView tvDesc;

    private MovieItem movieItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        //TODO : get Intent's Data
        Uri uri = getIntent().getData();
        if (uri != null){
            Cursor cursor = getContentResolver().query(uri,null,null,null,null);
            if (cursor != null){
                if(cursor.moveToFirst()) movieItem = new MovieItem(cursor);
                cursor.close();
            }
        }

        //TODO : assign into Components
        if (movieItem != null){
            tvTitle.setText(movieItem.getTitle());
            tvDate.setText(movieItem.getReleaseDate());
            tvRate.setText(String.valueOf(movieItem.getVoteAverage()));
            tvDesc.setText(movieItem.getOverview());
            tvLang.setText(movieItem.getOriginalLanguage());
            Glide.with(this)
                    .load("https://image.tmdb.org/t/p/w185" + movieItem.getPosterPath())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.blank_movie)
                            .error(R.drawable.blank_movie))
                    .into(imgPoster);
            Glide.with(this)
                    .load("https://image.tmdb.org/t/p/w780" + movieItem.getBackdropPath())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.blank_movie)
                            .error(R.drawable.blank_movie))
                    .into(imgBackdrop);
        }

        //TODO : enable Back button in ActionBar
        getSupportActionBar().setTitle(movieItem.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
