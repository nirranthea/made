package dicoding.made.favourite.database;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public final class DatabaseContract {
    private static String TABLE_NAME = "tablemovie";

    public static final class MovieColumns implements BaseColumns{

        public static String IDMOVIE = "idmovie";
        public static String TITLE = "title";
        public static String OVERVIEW = "overview";
        public static String RELEASEDATE = "releasedate";
        public static String RATE = "rate";
        public static String POSTERPATH = "posterpath";
        public static String BACKDROP = "backdrop";
        public static String ORIGINLANG = "originlang";
    }

    //TODO : Content-Provider
    private static final String AUTHORITY = "dicoding.made.sub5cataloguemovieextended";
    private static final String SCHEME = "content";
    public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
            .authority(AUTHORITY)
            .appendPath(TABLE_NAME)
            .build();

    public static String getColumnString(Cursor cursor, String columnName){
        return cursor.getString(cursor.getColumnIndex(columnName));
    }
    public static int getColumnInt(Cursor cursor, String columnName){
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }
    public static double getColumnDouble(Cursor cursor, String columnName){
        return cursor.getDouble(cursor.getColumnIndex(columnName));
    }
}
