package dicoding.made.favourite.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.favourite.R;

import static dicoding.made.favourite.database.DatabaseContract.MovieColumns.OVERVIEW;
import static dicoding.made.favourite.database.DatabaseContract.MovieColumns.POSTERPATH;
import static dicoding.made.favourite.database.DatabaseContract.MovieColumns.RELEASEDATE;
import static dicoding.made.favourite.database.DatabaseContract.MovieColumns.TITLE;
import static dicoding.made.favourite.database.DatabaseContract.getColumnString;


public class FavouriteAdapter extends CursorAdapter {

    @BindView(R.id.movie_poster) ImageView imgPoster;
    @BindView(R.id.movie_title) TextView tvTitle;
    @BindView(R.id.movie_description) TextView tvDesc;
    @BindView(R.id.movie_release) TextView tvRelease;

    public FavouriteAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_favourite,viewGroup,false);
        return view;
    }

    @Override
    public Cursor getCursor(){
        return super.getCursor();
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if (cursor != null){
            ButterKnife.bind(this,view);

            tvTitle.setText(getColumnString(cursor,TITLE));
            tvDesc.setText(getColumnString(cursor,OVERVIEW));
            tvRelease.setText(getColumnString(cursor,RELEASEDATE));
            Glide.with(context)
                    .load("https://image.tmdb.org/t/p/w185" + getColumnString(cursor,POSTERPATH))
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.movie_blank_portrait)
                            .error(R.drawable.movie_blank_portrait))
                    .into(imgPoster);
        }
    }
}
