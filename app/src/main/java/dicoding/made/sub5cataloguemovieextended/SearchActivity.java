package dicoding.made.sub5cataloguemovieextended;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.sub5cataloguemovieextended.adapter.RecyclerViewAdapter;
import dicoding.made.sub5cataloguemovieextended.model.Movie;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, MainView {

    private RecyclerViewAdapter mAdapter;
    private List<Movie> listMovies = new ArrayList<>();
    private SearchPresenter presenter;

    //TODO : use ButterKnife instead
    @BindView(R.id.edt_search) EditText edtSearch;
    @BindView(R.id.btn_cari) Button btnCari;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.rv_list) RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        //TODO : set Actionbar's Title
        getSupportActionBar().setTitle(getResources().getString(R.string.search_page_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //TODO : Initialize Recycler View & Adapter
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerViewAdapter(getApplicationContext(), listMovies);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        btnCari.setOnClickListener(this);

        //TODO : initialize Presenter
        presenter = new SearchPresenter(this);

        //TODO : handle if ENTER pressed in EditText
        edtSearch.setOnEditorActionListener(new OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO){
                    performSearch();
                    handled = true;
                }
                return handled;
            }
        });

        //TODO : saveInstanceState
        if (savedInstanceState != null){
            List<Movie> movies = savedInstanceState.getParcelableArrayList("listMovies");
            listMovies.addAll(movies);
            mAdapter.notifyDataSetChanged();
            String searchQuery = savedInstanceState.getString("searchQuery");
            edtSearch.setText(searchQuery);
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_cari){
            //TODO : perform click Button
            performSearch();
        }
    }

    private void performSearch() {
        String searchQuery = edtSearch.getText().toString();
        String langMode = getResources().getString(R.string.mode_bahasa);
        if (searchQuery.length() != 0){
            presenter.getMovieList(searchQuery,langMode);
        } else{
            Toast.makeText(this,getResources().getString(R.string.blank_searching),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMovieList(List<Movie> data) {
        listMovies.clear();
        listMovies.addAll(data);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //TODO : move to Language Setting
        if (id == R.id.action_settings) {
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        }
        else if (id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //TODO : saveInstanceState
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("listMovies", new ArrayList<>(listMovies));
        outState.putString("searchQuery", edtSearch.getText().toString());
    }

}
