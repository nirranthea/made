package dicoding.made.sub5cataloguemovieextended;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.sub5cataloguemovieextended.database.MovieHelper;
import dicoding.made.sub5cataloguemovieextended.model.Movie;

import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.CONTENT_URI;

public class DetailActivity extends AppCompatActivity {

    //TODO : use  ButterKnife instead
    @BindView(R.id.detail_backdrop) ImageView imgBackdrop;
    @BindView(R.id.detail_profpic) ImageView imgPoster;
    @BindView(R.id.detail_title) TextView tvTitle;
    @BindView(R.id.detail_date) TextView tvDate;
    @BindView(R.id.detail_language) TextView tvLang;
    @BindView(R.id.detail_rate) TextView tvRate;
    @BindView(R.id.detail_desc) TextView tvDesc;

    private boolean isFavourite = false;
    private Menu menuItem;

    private Movie movie;
    private MovieHelper movieHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        //TODO : get Intent's Extras
        //Change to Parcelable
        movie = getIntent().getParcelableExtra("movie");

        //TODO : assign extras into component
        double movieRate = movie.getVoteAverage();
        String movieTitle = movie.getTitle();
        String movieDate = movie.getReleaseDate();
        String movieOverview = movie.getOverview();
        String movieLang = movie.getOriginalLanguage();
        String moviePoster = movie.getPosterPath();
        String movieBackdrop = movie.getBackdropPath();

        tvTitle.setText(movieTitle);
        tvDate.setText(movieDate);
        tvRate.setText(String.valueOf(movieRate));
        tvDesc.setText(movieOverview);
        tvLang.setText(movieLang);
        Glide.with(this)
                .load("https://image.tmdb.org/t/p/w185" + moviePoster)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.blank_movie)
                        .error(R.drawable.blank_movie))
                .into(imgPoster);
        Glide.with(this)
                .load("https://image.tmdb.org/t/p/w780" + movieBackdrop)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.blank_movie)
                        .error(R.drawable.blank_movie))
                .into(imgBackdrop);

        //TODO : enable Back button in ActionBar
        getSupportActionBar().setTitle(movie.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //TODO : initialize MovieHelper
        movieHelper = new MovieHelper(this);

        favouriteState();

    }

    //TODO : check if movie is in Favourite List
    private void favouriteState() {
        movieHelper.open();
        Uri uri = getIntent().getData();
        if (uri != null){
            Cursor cursor = getContentResolver().query(uri,null,null,null,null);
            if (cursor.getCount() > 0){
                isFavourite = true;
                cursor.close();
            }
        }
        movieHelper.close();
    }

    private void setFavourite() {
        if (isFavourite){
            menuItem.getItem(0).setIcon(R.drawable.ic_star_fill);
        } else{
            menuItem.getItem(0).setIcon(R.drawable.ic_star_empty);
        }
    }

    private void removeFromFavourite() {
        movieHelper.open();
        getContentResolver().delete(getIntent().getData(),null,null);
        movieHelper.close();
        Toast.makeText(this, "Removed from Favorite", Toast.LENGTH_SHORT).show();
    }

    private void addToFavourite() {
        movieHelper.open();
        ContentValues values = movieHelper.convertToContentValues(movie);
        getContentResolver().insert(CONTENT_URI,values);
        movieHelper.close();
        Toast.makeText(this, "Added to Favorite", Toast.LENGTH_SHORT).show();
    }

    //TODO : perform Back button in ActionBar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        else if (item.getItemId() == R.id.action_favourite){
            if (isFavourite) {
                removeFromFavourite();
            } else{
                addToFavourite();
            }
            isFavourite = !isFavourite;
            setFavourite();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //TODO : inflate menu_details
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        menuItem = menu;
        setFavourite();
        return true;
    }


}
