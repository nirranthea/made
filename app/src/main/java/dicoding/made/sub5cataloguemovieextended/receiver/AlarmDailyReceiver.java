package dicoding.made.sub5cataloguemovieextended.receiver;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

import dicoding.made.sub5cataloguemovieextended.R;

public class AlarmDailyReceiver extends BroadcastReceiver {

    public static final String TYPE_REPEATING = "RepeatingAlarm";
    public static final String EXTRA_TYPE = "type";
    public static final String EXTRA_MESSAGE = "message";
    public static final int NOTIF_ID_REPEATING = 101;

    public AlarmDailyReceiver(){

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String message = intent.getStringExtra(EXTRA_MESSAGE);
        Log.d("alarm","isian message : "+message);
        String title = "Catalogue Movie";
        int notifId = NOTIF_ID_REPEATING;
        showAlarmNotification(context,title,message,notifId);
    }

    private void showAlarmNotification(Context context, String title, String message, int notifId) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,"ChannelId")
                .setSmallIcon(R.drawable.ic_info_black_24dp)
                .setContentTitle(title)
                .setLargeIcon(BitmapFactory
                        .decodeResource(context.getResources(),R.drawable.ic_info_black_24dp))
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000,1000,1000,1000,1000})
                .setSound(alarmSound);
        nm.notify(notifId,builder.build());
    }

    public void setDailyReminderAlarm(Context context, String type, String time, String message){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context,AlarmDailyReceiver.class);
        intent.putExtra(EXTRA_MESSAGE,message);
        intent.putExtra(EXTRA_TYPE,type);
        String timeArray[] = time.split(":");
        Calendar calendar = Calendar.getInstance();
        Calendar current = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND,0);
        int requestCode = NOTIF_ID_REPEATING;
        long intendedTime = calendar.getTimeInMillis();
        long currentTime = current.getTimeInMillis();

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,requestCode,intent,0);
        //TODO : if currentTime is passed alarm time, set Alarm for tomorrow
        if (intendedTime >= currentTime){
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,intendedTime,AlarmManager.INTERVAL_DAY,pendingIntent);
        } else {
            calendar.add(Calendar.DAY_OF_MONTH,1);
            intendedTime = calendar.getTimeInMillis();
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,intendedTime,AlarmManager.INTERVAL_DAY,pendingIntent);
        }
        Log.d("alarm","Daily Reminder set up " + timeArray[0] + timeArray[1]);
    }

    public void cancelDailyReminderAlarm(Context context, String type){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmDailyReceiver.class);
        int requestCode = NOTIF_ID_REPEATING;

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,requestCode,intent,0);
        alarmManager.cancel(pendingIntent);
        Log.d("alarm","Daily Reminder canceled");
    }

}
