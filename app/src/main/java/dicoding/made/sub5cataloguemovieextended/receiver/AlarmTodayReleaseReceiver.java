package dicoding.made.sub5cataloguemovieextended.receiver;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.os.Handler;

import dicoding.made.sub5cataloguemovieextended.BuildConfig;
import dicoding.made.sub5cataloguemovieextended.R;
import dicoding.made.sub5cataloguemovieextended.api.ApiClient;
import dicoding.made.sub5cataloguemovieextended.model.Movie;
import dicoding.made.sub5cataloguemovieextended.model.MovieResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlarmTodayReleaseReceiver extends BroadcastReceiver {

    public static final String TYPE_REPEATING2 = "RepeatingAlarm";
    public static final String EXTRA_TYPE = "type";
    public static final String EXTRA_MESSAGE = "message";
    public static final int NOTIF_ID_REPEATING2 = 102;

    private List<Movie> list = new ArrayList<>();
    private Handler handler = new Handler();
    private Context mContext;

    public AlarmTodayReleaseReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        loadData();
        this.mContext = context;
        //TODO : using Handler to wait till loadData finished, approx. 5 seconds
        handler.postDelayed(runnable, 5000);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            String title = "Catalogue Movie";
            String message = insertMessage(list) + " Release Today";
            Log.d("alarm", "isian message : " + message);
            int notifId = NOTIF_ID_REPEATING2;
            showAlarmNotification(mContext, title, message, notifId);
        }
    };

    private void showAlarmNotification(Context context, String title, String message, int notifId) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "ChannelId")
                .setSmallIcon(R.drawable.ic_info_black_24dp)
                .setContentTitle(title)
                .setLargeIcon(BitmapFactory
                        .decodeResource(context.getResources(), R.drawable.ic_info_black_24dp))
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound);
        nm.notify(notifId, builder.build());
    }

    private void loadData() {
        String apiKey = BuildConfig.ApiKey;
        ApiClient.get().getMovie("upcoming", apiKey, "en-Us")
                .enqueue(new Callback<MovieResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                        List<Movie> movies = response.body().getResults();
                        list.addAll(movies);
                        Log.d("alarm", "list = " + list.toString());
                    }

                    @Override
                    public void onFailure(@NonNull Call<MovieResponse> call, @NonNull Throwable t) {

                    }
                });
    }

    private String insertMessage(List<Movie> list) {
        String titleMovie = "";
        List<Movie> listMovie = new ArrayList<>(list);
        Log.d("alarm", "listMovie = " + listMovie.toString());
        boolean isFound = false;

        for (int i = 0; i < listMovie.size(); i++) {
            String releaseDate = listMovie.get(i).getReleaseDate();
            Log.d("alarm", "releaseDate: " + releaseDate);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date strDate = new Date();
            try {
                strDate = sdf.parse(releaseDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.d("alarm", "strDate: " + strDate.getTime());
            Log.d("alarm", "isToday " + DateUtils.isToday(strDate.getTime()));
            if (DateUtils.isToday(strDate.getTime())) {
                titleMovie = listMovie.get(i).getTitle();
                isFound = true;
                Log.d("alarm", "today found: " + titleMovie);
                break;
            }
        }
        if (!isFound){
            titleMovie = "No Movie";
        }
        return titleMovie;
    }

    public void setReleaseTodayReminderAlarm(Context context, String type, String time) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmTodayReleaseReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, "message");
        intent.putExtra(EXTRA_TYPE, type);
        String timeArray[] = time.split(":");
        Calendar calendar = Calendar.getInstance();
        Calendar current = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);
        int requestCode = NOTIF_ID_REPEATING2;
        long intendedTime = calendar.getTimeInMillis();
        long currentTime = current.getTimeInMillis();

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        //TODO : if currentTime is passed alarm time, dont sent reminder for today
        if (intendedTime >= currentTime) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, intendedTime, AlarmManager.INTERVAL_DAY, pendingIntent);
        }
        Log.d("alarm","Today Release Reminder set up " + timeArray[0] + timeArray[1]);
    }

    public void cancelTodayReleaseReminderAlarm(Context context, String type) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmDailyReceiver.class);
        int requestCode = NOTIF_ID_REPEATING2;

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        alarmManager.cancel(pendingIntent);
        Log.d("alarm","Today Release Reminder canceled");
    }
}
