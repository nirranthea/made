package dicoding.made.sub5cataloguemovieextended;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import dicoding.made.sub5cataloguemovieextended.api.ApiClient;
import dicoding.made.sub5cataloguemovieextended.model.Movie;
import dicoding.made.sub5cataloguemovieextended.model.MovieResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class SearchPresenter {

    private MainView view;

    SearchPresenter(MainView view){
        this.view = view;
    }

    void getMovieList(String query, String langMode){
        view.showLoading();
        String apiKey = BuildConfig.ApiKey;
        Log.d("apiKey",apiKey);

        try{
            ApiClient.get().getSearchMovie("movie",apiKey,langMode,query)
                    .enqueue(new Callback<MovieResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                            Log.d("Retrofit", response.body().toString());
                            List<Movie> movies = response.body().getResults();
                            view.hideLoading();
                            if (movies != null){
                                view.showMovieList(movies);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<MovieResponse> call, @NonNull Throwable t) {
                            Log.d("Retrofit", "failure : " + t.getMessage());
                            view.hideLoading();
                        }
                    });

        } catch (Exception e){
            e.printStackTrace();
            Log.d("Retrofit", "failure : " + e.getMessage());
        }
    }

}
