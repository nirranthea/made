package dicoding.made.sub5cataloguemovieextended;

import java.util.List;

import dicoding.made.sub5cataloguemovieextended.model.Movie;

public interface MainView {

    void showLoading();
    void hideLoading();
    void showMovieList(List<Movie> data);

}
