package dicoding.made.sub5cataloguemovieextended;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.sub5cataloguemovieextended.adapter.RecyclerViewAdapter;
import dicoding.made.sub5cataloguemovieextended.model.Movie;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment implements MainView {

    String movieCategory = "";
    private RecyclerViewAdapter adapter;
    private List<Movie> listMovies = new ArrayList<>();

    //TODO : use ButterKnife instead
    @BindView(R.id.progressBar_movie) ProgressBar progressBar;
    @BindView(R.id.rv_movie) RecyclerView recyclerView;

    public MovieFragment () {
        // Required empty public constructor
    }

    public static MovieFragment newInstance(String category) {
        Bundle args = new Bundle();
        args.putString("category",category);
        MovieFragment fragment = new MovieFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            movieCategory = getArguments().getString("category");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        ButterKnife.bind(this,view);

        //TODO : Initialize Recycler View & Adapter
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new RecyclerViewAdapter(getContext(), listMovies);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        String langMode = getResources().getString(R.string.mode_bahasa);

        if (savedInstanceState == null) {
            //TODO : initialize Presenter
            MoviePresenter presenter = new MoviePresenter(this);
            presenter.getMovieList(movieCategory, langMode);
        }
        else {
            //TODO : if saveInstanceState not null, don't load from API
            List<Movie> movies = savedInstanceState.getParcelableArrayList("listMovies");
            listMovies.addAll(movies);
            adapter.notifyDataSetChanged();
        }

        return view;
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMovieList(List<Movie> data) {
        listMovies.clear();
        listMovies.addAll(data);
        adapter.notifyDataSetChanged();
    }

    //TODO : saveInstanceState
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("listMovies", new ArrayList<>(listMovies));
    }

}
