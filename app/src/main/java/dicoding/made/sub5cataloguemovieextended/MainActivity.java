package dicoding.made.sub5cataloguemovieextended;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.sub5cataloguemovieextended.adapter.TabFragmentPagerAdapter;
import dicoding.made.sub5cataloguemovieextended.pref.UserPreference;
import dicoding.made.sub5cataloguemovieextended.receiver.AlarmDailyReceiver;
import dicoding.made.sub5cataloguemovieextended.receiver.AlarmTodayReleaseReceiver;

import static dicoding.made.sub5cataloguemovieextended.util.Util.executeDailyReminder;
import static dicoding.made.sub5cataloguemovieextended.util.Util.executeTodayReleaseReminder;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //TODO : use ButterKnife instead
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.pager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabs;

    private UserPreference mUserPreference;
    private AlarmDailyReceiver alarmDailyReceiver;
    private AlarmTodayReleaseReceiver alarmTodayReleaseReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true); //default

        viewPager.setAdapter(new TabFragmentPagerAdapter(getSupportFragmentManager(),this));
        tabs.setTabTextColors(getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(android.R.color.white));
        tabs.setupWithViewPager(viewPager);
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        alarmDailyReceiver = new AlarmDailyReceiver();
        alarmTodayReleaseReceiver = new AlarmTodayReleaseReceiver();
        mUserPreference = new UserPreference(this);

        //TODO : start Alarm Manager if Alarm setting is On
        if (mUserPreference.getDailyReminder()) {
            executeDailyReminder(this, mUserPreference, alarmDailyReceiver);
        }
        if (mUserPreference.getReleaseReminder()){
            executeTodayReleaseReminder(this,mUserPreference,alarmTodayReleaseReceiver);
        }

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //TODO : move to Language Setting
        if (id == R.id.action_settings) {
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_search) {
            //GO to Search Activity
            Intent searchIntent = new Intent(MainActivity.this,SearchActivity.class);
            startActivity(searchIntent);
        } else if (id == R.id.nav_setting) {
            //Go to Setting Activity
            Intent settingIntent = new Intent(MainActivity.this,SettingsActivity.class);
            startActivity(settingIntent);
        } else if (id == R.id.nav_favourite){
            //Go to Favourite Activity
            Intent favouriteIntent = new Intent(MainActivity.this,FavouriteActivity.class);
            startActivity(favouriteIntent);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(0).setChecked(true); //default position
    }


}
