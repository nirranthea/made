package dicoding.made.sub5cataloguemovieextended.util;

import android.content.Context;
import android.util.Log;

import dicoding.made.sub5cataloguemovieextended.R;
import dicoding.made.sub5cataloguemovieextended.receiver.AlarmDailyReceiver;
import dicoding.made.sub5cataloguemovieextended.receiver.AlarmTodayReleaseReceiver;
import dicoding.made.sub5cataloguemovieextended.pref.UserPreference;

public class Util {

    public static void executeDailyReminder(Context context, UserPreference mUserPreference, AlarmDailyReceiver alarmDailyReceiver) {
        Log.d("alarm", "execute Daily Reminder");
        //Daily Reminder every 07.00
        if (mUserPreference.getDailyReminder()) {
            alarmDailyReceiver.setDailyReminderAlarm(context, AlarmDailyReceiver.TYPE_REPEATING,
                    "07:00", context.getString(R.string.text_daily_reminder));
        } else {
            alarmDailyReceiver.cancelDailyReminderAlarm(context, AlarmDailyReceiver.TYPE_REPEATING);
        }
    }

    public static void executeTodayReleaseReminder(Context context, UserPreference mUserPreference, AlarmTodayReleaseReceiver alarmReceiver){
        Log.d("alarm", "execute Today Release Reminder");
        //Today Release Reminder every 08.00
        if (mUserPreference.getReleaseReminder()) {
            alarmReceiver.setReleaseTodayReminderAlarm(context, AlarmTodayReleaseReceiver.TYPE_REPEATING2,
                    "08:00");
        } else {
            alarmReceiver.cancelTodayReleaseReminderAlarm(context, AlarmTodayReleaseReceiver.TYPE_REPEATING2);
        }
    }

}