package dicoding.made.sub5cataloguemovieextended.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "dbfavouritemovie";
    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_TABLE_MOVIE = String.format("CREATE TABLE %s"
                    + " (%s INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + " %s TEXT NOT NULL,"
                    + " %s TEXT,"
                    + " %s TEXT,"
                    + " %s TEXT,"
                    + " %s TEXT,"
                    + " %s TEXT,"
                    + " %s DOUBLE,"
                    + " %s INTEGER NOT NULL)",
            DatabaseContract.TABLE_NAME,
            DatabaseContract.MovieColumns._ID,
            DatabaseContract.MovieColumns.TITLE,
            DatabaseContract.MovieColumns.OVERVIEW,
            DatabaseContract.MovieColumns.ORIGINLANG,
            DatabaseContract.MovieColumns.POSTERPATH,
            DatabaseContract.MovieColumns.BACKDROP,
            DatabaseContract.MovieColumns.RELEASEDATE,
            DatabaseContract.MovieColumns.RATE,
            DatabaseContract.MovieColumns.IDMOVIE
    );

    DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_MOVIE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TABLE_NAME);
        onCreate(db);
    }
}
