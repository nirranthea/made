package dicoding.made.sub5cataloguemovieextended.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import dicoding.made.sub5cataloguemovieextended.model.Movie;

import static android.provider.BaseColumns._ID;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.BACKDROP;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.IDMOVIE;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.ORIGINLANG;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.OVERVIEW;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.POSTERPATH;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.RATE;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.RELEASEDATE;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.TITLE;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.TABLE_NAME;

public class MovieHelper {
    private static String DATABASE_TABLE = TABLE_NAME;
    private Context context;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    public MovieHelper(Context context){
        this.context = context;
    }

    public MovieHelper open() throws SQLException{
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        databaseHelper.close();
    }

    //METHOD FOR QUERY IN SQLITE
    public ArrayList<Movie> query(){
        ArrayList<Movie> arrayList = new ArrayList<Movie>();
        Cursor cursor = database.query(DATABASE_TABLE,
                null,
                null,
                null,
                null,
                null, _ID + " DESC",
                null);
        cursor.moveToFirst();
        Movie movie;
        Log.d("fav","Jumlah cursor = "+cursor.getCount());
        if (cursor.getCount() > 0){
            do{
                movie = new Movie();
                movie.setDbId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                Log.d("fav","nilai _ID = "+cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));
                movie.setPosterPath(cursor.getString(cursor.getColumnIndexOrThrow(POSTERPATH)));
                movie.setBackdropPath(cursor.getString(cursor.getColumnIndexOrThrow(BACKDROP)));
                movie.setOriginalLanguage(cursor.getString(cursor.getColumnIndexOrThrow(ORIGINLANG)));
                movie.setVoteAverage(cursor.getDouble(cursor.getColumnIndexOrThrow(RATE)));
                movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(IDMOVIE)));
                movie.setReleaseDate(cursor.getString(cursor.getColumnIndexOrThrow(RELEASEDATE)));

                arrayList.add(movie);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public long insert(Movie movie){
        ContentValues initialValues = new ContentValues();
        initialValues.put(TITLE, movie.getTitle());
        initialValues.put(OVERVIEW,movie.getOverview());
        initialValues.put(POSTERPATH,movie.getPosterPath());
        initialValues.put(BACKDROP,movie.getBackdropPath());
        initialValues.put(ORIGINLANG,movie.getOriginalLanguage());
        initialValues.put(RATE,String.valueOf(movie.getVoteAverage()));
        initialValues.put(IDMOVIE,String.valueOf(movie.getId()));
        initialValues.put(RELEASEDATE,movie.getReleaseDate());
        return database.insert(DATABASE_TABLE,null,initialValues);
    }

    public int delete(int id){
//        return database.delete(TABLE_NAME,_ID + " = '" + id + "'", null);
        return database.delete(TABLE_NAME,IDMOVIE + " = '" + id + "'", null);
    }

    public Movie queryById(String id){
        Cursor cursor = null;
        cursor = database.query(DATABASE_TABLE,
                null,
                IDMOVIE + " = ?",
                new String[]{id},
                null,
                null,
                null,
                null);
        cursor.moveToFirst();
        Movie result = new Movie();
        Log.d("fav","count cursor byId: "+cursor.getCount());
        if (cursor.getCount() > 0) {
            result.setId(cursor.getInt(cursor.getColumnIndexOrThrow(IDMOVIE)));
            cursor.close();
            return result;
        }
        else{
            cursor.close();
            return null;
        }
    }


    //TODO : Content-Provider
    //METHOD FOR QUERY IN PROVIDER
    public Cursor queryByIdProvider(String id){
        return database.query(DATABASE_TABLE,null,
                IDMOVIE + " = ?",
                new String[]{id},
                null,
                null,
                null,
                null);
    }

    public Cursor queryProvider(){
        return database.query(DATABASE_TABLE,
                null,
                null,
                null,
                null,
                null,
                _ID + " DESC");
    }

    public long insertProvider(ContentValues values){
        return database.insert(DATABASE_TABLE,null,values);
    }

    public int updateProvider(String id, ContentValues values){
        return database.update(DATABASE_TABLE,values,_ID + " = ?",new String[]{id});
    }

    public int deleteProvider(String id){
        return database.delete(DATABASE_TABLE, IDMOVIE + " = ?",new String[]{id});
    }

    public ContentValues convertToContentValues(Movie movie){
        ContentValues initialValues = new ContentValues();
        initialValues.put(TITLE, movie.getTitle());
        initialValues.put(OVERVIEW,movie.getOverview());
        initialValues.put(POSTERPATH,movie.getPosterPath());
        initialValues.put(BACKDROP,movie.getBackdropPath());
        initialValues.put(ORIGINLANG,movie.getOriginalLanguage());
        initialValues.put(RATE,String.valueOf(movie.getVoteAverage()));
        initialValues.put(IDMOVIE,String.valueOf(movie.getId()));
        initialValues.put(RELEASEDATE,movie.getReleaseDate());
        return initialValues;
    }
}
