package dicoding.made.sub5cataloguemovieextended;

import android.content.Intent;
import android.database.Cursor;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.sub5cataloguemovieextended.adapter.RecyclerViewAdapterFavourite;

import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.CONTENT_URI;

public class FavouriteActivity extends AppCompatActivity {

    @BindView(R.id.rv_movie_fav) RecyclerView recyclerViewFav;
    @BindView(R.id.progressBar_movie_fav) ProgressBar progressBarFav;

    private RecyclerViewAdapterFavourite adapterFav;
    private Cursor listFav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        ButterKnife.bind(this);

        recyclerViewFav.setHasFixedSize(true);
        recyclerViewFav.setLayoutManager(new LinearLayoutManager(this));
        adapterFav = new RecyclerViewAdapterFavourite(this);
        recyclerViewFav.setAdapter(adapterFav);
        adapterFav.notifyDataSetChanged();

        //TODO : set Actionbar's Title
        getSupportActionBar().setTitle(getResources().getString(R.string.favourite_page_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void showFavourite(){
        listFav = getContentResolver().query(CONTENT_URI,null,null,null,null);
        adapterFav.setListMovie(listFav);
        adapterFav.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //TODO : move to Language Setting
        if (id == R.id.action_settings) {
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        }
        else if (id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listFav = null;
        showFavourite();
    }
}
