package dicoding.made.sub5cataloguemovieextended;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.sub5cataloguemovieextended.pref.UserPreference;
import dicoding.made.sub5cataloguemovieextended.receiver.AlarmDailyReceiver;
import dicoding.made.sub5cataloguemovieextended.receiver.AlarmTodayReleaseReceiver;

import static dicoding.made.sub5cataloguemovieextended.util.Util.executeDailyReminder;
import static dicoding.made.sub5cataloguemovieextended.util.Util.executeTodayReleaseReminder;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.switch1)
    Switch mSwitch1;
    @BindView(R.id.switch2)
    Switch mSwitch2;
    @BindView(R.id.btn_lang)
    ImageView btnLang;

    private UserPreference mUserPreference;
    private AlarmDailyReceiver alarmDailyReceiver;
    private AlarmTodayReleaseReceiver alarmTodayReleaseReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        alarmDailyReceiver = new AlarmDailyReceiver();
        alarmTodayReleaseReceiver = new AlarmTodayReleaseReceiver();

        //TODO : set Actionbar's Title
        getSupportActionBar().setTitle(getResources().getString(R.string.action_settings));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //TODO : initialize switches
        mUserPreference = new UserPreference(this);
        if (!mUserPreference.getDailyReminder()) {
            mSwitch1.setChecked(false);
        } else {
            mSwitch1.setChecked(true);
        }
        if (!mUserPreference.getReleaseReminder()) {
            mSwitch2.setChecked(false);
        } else {
            mSwitch2.setChecked(true);
        }

        //TODO : set Switch Listener
        mSwitch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mUserPreference.setDailyReminder(true);
                    Toast.makeText(getBaseContext(), "Daily Reminder is set ", Toast.LENGTH_SHORT).show();
                    executeDailyReminder(getBaseContext(), mUserPreference, alarmDailyReceiver);
                } else {
                    mUserPreference.setDailyReminder(false);
                    Toast.makeText(getBaseContext(), "Daily Reminder is disabled", Toast.LENGTH_SHORT).show();
                    executeDailyReminder(getBaseContext(), mUserPreference, alarmDailyReceiver);
                }
            }
        });
        mSwitch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mUserPreference.setReleaseTodayReminder(true);
                    Toast.makeText(getBaseContext(), "Release Today Reminder is set", Toast.LENGTH_SHORT).show();
                    executeTodayReleaseReminder(getBaseContext(),mUserPreference,alarmTodayReleaseReceiver);
                } else {
                    mUserPreference.setReleaseTodayReminder(false);
                    Toast.makeText(getBaseContext(), "Release Today Reminder is disabled", Toast.LENGTH_SHORT).show();
                    executeTodayReleaseReminder(getBaseContext(),mUserPreference,alarmTodayReleaseReceiver);
                }
            }
        });

        btnLang.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        //TODO : go to Language Settings
        if (v.getId() == R.id.btn_lang) {
            Intent langIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(langIntent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
