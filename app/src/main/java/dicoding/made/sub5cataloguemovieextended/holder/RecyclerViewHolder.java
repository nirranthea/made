package dicoding.made.sub5cataloguemovieextended.holder;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.sub5cataloguemovieextended.DetailActivity;
import dicoding.made.sub5cataloguemovieextended.R;
import dicoding.made.sub5cataloguemovieextended.model.Movie;

import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.CONTENT_URI;

public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public String imgBackdrop, imgPoster, language;
    public int id_film,id_db;
    public double rate;

    //TODO : use ButterKnife instead
    @BindView(R.id.movie_poster) public ImageView imagePoster;
    @BindView(R.id.movie_title) public TextView tvTitle;
    @BindView(R.id.movie_description) public TextView tvDesc;
    @BindView(R.id.movie_release) public TextView tvDate;

    public RecyclerViewHolder(View itemView){
        super(itemView);
        itemView.setOnClickListener(this);
        ButterKnife.bind(this,itemView);
    }

    //bindMovie method to be used in parcelable
    private Movie bindMovie(){
        Movie movie = new Movie();
        movie.setDbId(id_db);
        movie.setId(id_film);
        movie.setTitle(tvTitle.getText().toString());
        movie.setOverview(tvDesc.getText().toString());
        movie.setReleaseDate(tvDate.getText().toString());
        movie.setVoteAverage(rate);
        movie.setBackdropPath(imgBackdrop);
        movie.setPosterPath(imgPoster);
        movie.setOriginalLanguage(language);
        return movie;
    }

    @Override
    public void onClick(View v) {
        //TODO : intent for Detail Movie
        Intent intent = new Intent(v.getContext(), DetailActivity.class);
        //Change to parcelable
        Movie resultMovie = bindMovie();
        intent.putExtra("movie",resultMovie);
        //TODO : Content-Provider
        Uri uri = Uri.parse(CONTENT_URI + "/" + id_film);
        intent.setData(uri);
        v.getContext().startActivity(intent);
    }
}
