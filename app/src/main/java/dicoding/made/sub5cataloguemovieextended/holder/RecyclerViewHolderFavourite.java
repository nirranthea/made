package dicoding.made.sub5cataloguemovieextended.holder;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dicoding.made.sub5cataloguemovieextended.DetailActivity;
import dicoding.made.sub5cataloguemovieextended.R;
import dicoding.made.sub5cataloguemovieextended.model.Movie;

import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.CONTENT_URI;

public class RecyclerViewHolderFavourite extends RecyclerView.ViewHolder implements View.OnClickListener {

    public String imgBackdropFav, imgPosterFav, languageFav;
    public int id_filmFav,id_dbFav;
    public double rateFav;

    //TODO : use ButterKnife instead
    @BindView(R.id.movie_poster) public ImageView imagePosterFav;
    @BindView(R.id.movie_title) public TextView tvTitleFav;
    @BindView(R.id.movie_description) public TextView tvDescFav;
    @BindView(R.id.movie_release) public TextView tvDateFav;

    public RecyclerViewHolderFavourite(View itemView){
        super(itemView);
        itemView.setOnClickListener(this);
        ButterKnife.bind(this,itemView);
    }

    //bindMovie method to be used in parcelable
    private Movie bindMovie(){
        Movie movie = new Movie();
        movie.setDbId(id_dbFav);
        movie.setId(id_filmFav);
        movie.setTitle(tvTitleFav.getText().toString());
        movie.setOverview(tvDescFav.getText().toString());
        movie.setReleaseDate(tvDateFav.getText().toString());
        movie.setVoteAverage(rateFav);
        movie.setBackdropPath(imgBackdropFav);
        movie.setPosterPath(imgPosterFav);
        movie.setOriginalLanguage(languageFav);
        return movie;
    }

    @Override
    public void onClick(View v) {
        //TODO : intent for Detail Movie
        Intent intent = new Intent(v.getContext(), DetailActivity.class);
        //Change to parcelable
        Movie resultMovie = bindMovie();
        intent.putExtra("movie",resultMovie);
        //TODO : Content-Provider
        Uri uri = Uri.parse(CONTENT_URI + "/" + id_filmFav);
        intent.setData(uri);
        v.getContext().startActivity(intent);
    }
}
