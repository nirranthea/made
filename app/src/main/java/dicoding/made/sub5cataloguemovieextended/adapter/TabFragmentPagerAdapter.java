package dicoding.made.sub5cataloguemovieextended.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import dicoding.made.sub5cataloguemovieextended.MovieFragment;
import dicoding.made.sub5cataloguemovieextended.R;

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public TabFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = MovieFragment.newInstance("now_playing");
                break;
            case 1:
                fragment = MovieFragment.newInstance("upcoming");
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2; //There are 2 Tabs
    }

    @Override
    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return context.getResources().getString(R.string.tab_title1);
            case 1:
                return context.getResources().getString(R.string.tab_title2);
        }
        return null;
    }

}
