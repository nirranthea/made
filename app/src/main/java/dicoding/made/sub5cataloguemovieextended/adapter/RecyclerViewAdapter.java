package dicoding.made.sub5cataloguemovieextended.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import dicoding.made.sub5cataloguemovieextended.R;
import dicoding.made.sub5cataloguemovieextended.holder.RecyclerViewHolder;
import dicoding.made.sub5cataloguemovieextended.model.Movie;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    private Context context;
    private List<Movie> movieList;

    public RecyclerViewAdapter(Context context, List<Movie> movieList){
        this.context = context;
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_movie,null);
        RecyclerViewHolder holder = new RecyclerViewHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w185" + movieList.get(position).getPosterPath())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.movie_blank_portrait)
                        .error(R.drawable.movie_blank_portrait))
                .into(holder.imagePoster);
        holder.tvTitle.setText(movieList.get(position).getTitle());
        holder.tvDate.setText(movieList.get(position).getReleaseDate());
        holder.tvDesc.setText(movieList.get(position).getOverview());
        holder.imgBackdrop = movieList.get(position).getBackdropPath();
        holder.imgPoster = movieList.get(position).getPosterPath();
        holder.id_film = movieList.get(position).getId();
        holder.rate = movieList.get(position).getVoteAverage();
        holder.language = movieList.get(position).getOriginalLanguage();
        holder.id_db = movieList.get(position).getDbId();
    }

    @Override
    public int getItemCount() {
        return this.movieList.size();
    }

}
