package dicoding.made.sub5cataloguemovieextended.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import dicoding.made.sub5cataloguemovieextended.R;
import dicoding.made.sub5cataloguemovieextended.holder.RecyclerViewHolderFavourite;
import dicoding.made.sub5cataloguemovieextended.model.Movie;

public class RecyclerViewAdapterFavourite extends RecyclerView.Adapter<RecyclerViewHolderFavourite> {

    private Context context;
    private Cursor movieList;

    public RecyclerViewAdapterFavourite(Context context){
        this.context = context;
    }

    public void setListMovie(Cursor listMovie){
        this.movieList = listMovie;
    }

    @NonNull
    @Override
    public RecyclerViewHolderFavourite onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_movie,null);
        RecyclerViewHolderFavourite holder = new RecyclerViewHolderFavourite(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolderFavourite holder, int position) {
        final Movie movie = getItem(position);
        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w185" + movie.getPosterPath())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.movie_blank_portrait)
                        .error(R.drawable.movie_blank_portrait))
                .into(holder.imagePosterFav);
        holder.tvTitleFav.setText(movie.getTitle());
        holder.tvDateFav.setText(movie.getReleaseDate());
        holder.tvDescFav.setText(movie.getOverview());
        holder.imgBackdropFav = movie.getBackdropPath();
        holder.imgPosterFav = movie.getPosterPath();
        holder.id_filmFav = movie.getId();
        holder.rateFav = movie.getVoteAverage();
        holder.languageFav = movie.getOriginalLanguage();
        holder.id_dbFav = movie.getDbId();

    }

    @Override
    public int getItemCount() {
        //TODO : Content-Provider
        return this.movieList.getCount();
    }

    //TODO : Content-Provider
    private Movie getItem(int position){
        if (!movieList.moveToPosition(position)){
            throw new IllegalStateException("Position Invalid");
        }
        return new Movie(movieList);
    }
}
