package dicoding.made.sub5cataloguemovieextended.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import dicoding.made.sub5cataloguemovieextended.R;
import dicoding.made.sub5cataloguemovieextended.model.Movie;

import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.CONTENT_URI;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.BACKDROP;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.POSTERPATH;
import static dicoding.made.sub5cataloguemovieextended.database.DatabaseContract.MovieColumns.TITLE;

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private List<Movie> widgetItems = new ArrayList<>();
    private Context mContext;
    private int mAppWidgetId;
    private Cursor cursor;

    public StackRemoteViewsFactory(Context context, Intent intent) {
        this.mContext = context;
        mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        final long identityToken = Binder.clearCallingIdentity();

        cursor = mContext.getContentResolver().query(CONTENT_URI, null, null, null, null);
        cursor.moveToFirst();
        Movie movie;
        if (cursor.getCount() > 0) {
            do {
                movie = new Movie();
                movie.setBackdropPath(cursor.getString(cursor.getColumnIndexOrThrow(BACKDROP)));
                movie.setPosterPath(cursor.getString(cursor.getColumnIndexOrThrow(POSTERPATH)));
                movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                widgetItems.add(movie);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();

        Binder.restoreCallingIdentity(identityToken);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return widgetItems.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);
        Bitmap bmp = null;
        String backdropPath = widgetItems.get(position).getBackdropPath();
        String movieTitle = widgetItems.get(position).getTitle();
        try {
            bmp = Glide.with(mContext)
                    .asBitmap()
                    .load("https://image.tmdb.org/t/p/w780" + backdropPath)
                    .apply(new RequestOptions()
                            .error(new ColorDrawable(mContext.getResources().getColor(R.color.colorPrimaryDark))))
                    .submit()
                    .get();

        } catch (InterruptedException | ExecutionException e) {
            Log.d("Widget Load Error", "error");
        }

        rv.setImageViewBitmap(R.id.imageView, bmp);
        rv.setTextViewText(R.id.tv_widget,movieTitle);

        Bundle extras = new Bundle();
        extras.putInt(MovieBannerWidget.EXTRA_ITEM, position);
        Intent fillIntent = new Intent();
        fillIntent.putExtras(extras);
        rv.setOnClickFillInIntent(R.id.imageView, fillIntent);
        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
