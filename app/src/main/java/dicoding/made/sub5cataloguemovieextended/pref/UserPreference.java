package dicoding.made.sub5cataloguemovieextended.pref;

import android.content.Context;
import android.content.SharedPreferences;

public class UserPreference {
    private String KEY_DAILY_REMINDER = "dailyreminder";
    private String KEY_RELEASE_TODAY_REMINDER = "releasetodayreminder";

    private SharedPreferences preferences;

    public UserPreference(Context context){
        String PREFS_NAME = "UserPref";
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void setDailyReminder(boolean dailyReminder){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_DAILY_REMINDER,dailyReminder);
        editor.apply();
    }

    public boolean getDailyReminder() {
        return preferences.getBoolean(KEY_DAILY_REMINDER, false);
    }

    public boolean getReleaseReminder(){
        return preferences.getBoolean(KEY_RELEASE_TODAY_REMINDER,false);
    }

    public void setReleaseTodayReminder(boolean releaseTodayReminder) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_RELEASE_TODAY_REMINDER, releaseTodayReminder);
        editor.apply();
    }
}
