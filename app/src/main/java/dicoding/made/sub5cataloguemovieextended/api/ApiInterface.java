package dicoding.made.sub5cataloguemovieextended.api;

import dicoding.made.sub5cataloguemovieextended.model.MovieResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("https://api.themoviedb.org/3/search/{movie_id}")
    Call<MovieResponse> getSearchMovie(@Path("movie_id") String movie_id, @Query("api_key") String api_key,
                                       @Query("language") String language, @Query("query") String query);

    @GET("https://api.themoviedb.org/3/movie/{movie_id}")
    Call<MovieResponse> getMovie(@Path("movie_id") String movie_id, @Query("api_key") String api_key,
                                      @Query("language") String language);

}
